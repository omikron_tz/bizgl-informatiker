# BIZ GL Berufsvorstellung Informatiker

## Ablauf
1.  Begrüssung / kurze Info MiZ
2.  Präsentation
3.  praktische Arbeit
4.  Präsentation
5.  evtl. Interview falls Zeit reicht

## praktische Arbeit
1.  Raspi kurz erklören
  - CPU / Anschlüsse / Netzteil
2.  Raspi anschliessen/aufstarten
3.  Raspi Config durchführen
  - Achtung keine Updates installieren (dauert zu lange)
3.  Internet verbinden/testen
  - W-LAN
4.  Terminal öffnen

cd Downloads/
git clone https://gitlab.com/omikron_tz/bizgl-informatiker
cd bizgl-informatiker
chmod +x *.sh
sudo ./service_install.sh

5.  Zuriff testen an externem PC
  - Was ist HTML? Wie wird die Verbindung hergestellt? (HTTP)

6.  nano /var/www/html/index.html
  - Datei anpassen (h1, p, img)
  - CTRL + O
  - CTRL + X

7.  Zugriff testen, was ist anders?

## Material
- 5 Bildschirme HDMI Kabel
- 5 Raspis
- 5 SD Karten mit Raspbian
- Test PCs
