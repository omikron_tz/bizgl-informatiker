#!/bin/bash

# Script Head

# created at:   2021-08-25
# created by:   Tobias Zweifel | Omikron Data AG
# description:  Installieren, Aktualisieren und Grundkonfiguraiton des Webservers
# version:      0.1

# check if script is run as root
# https://stackoverflow.com/questions/18215973/how-to-check-if-running-as-root-in-a-bash-script
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# update repos
apt update
# apt upgrade -y
# apt autoremove -y

# install required components
# check if apache is allready installed
dpkg -s "apache2" &> /dev/null

if [ $? -ne 0 ]
    then
        echo "not installed"
        apt install apache2 -y
    else
        echo "installed"
fi

# check if package "wget" is installed
# https://askubuntu.com/questions/1103860/script-to-check-if-some-program-is-already-installed
dpkg -s "wget" &> /dev/null

if [ $? -ne 0 ]
    then
        echo "not installed"
        apt install wget -y
    else
        echo "installed"
fi

# move default apache html folder
mv /var/www/html/index.html /var/www/html/index.html.org

#check if demo_page folder exists
if [ -d "./demo_page" ]
  then
    cp ./demo_page/index.html /var/www/html/
  else
    echo -e "${RED}Datei konnte nicht kopiert werden"
fi

# get current ip and set var
current_ip=$(hostname -I)
# show current ip for accessing website
echo "--------------------------------------------------------------------------------"
echo ""
echo "Du kannst deine Webseite nun über folgende Adresse aufrufen:"
echo -e "${GREEN}http://$current_ip"
echo ""
echo "--------------------------------------------------------------------------------"
