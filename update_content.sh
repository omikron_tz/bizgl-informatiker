#!/bin/bash

# Script Head

# created at:   2021-08-26
# created by:   Tobias Zweifel | Omikron Data AG
# description:  Anpassung der Webseite
# version:      0.1

# set variables
current_time=$(date +"%m-%d-%Y_%T")

# move index.html
mv /var/www/html/index.html /var/www/html/index.html.backup

# check if demo_page folder exists
if [ -d "./demo_page" ]
  then
    cp /var/www/html/index.html /var/www/html/index.html_backup_$current_time
  else
    echo -e "${RED}Datei konnte nicht kopiert werden"
fi
